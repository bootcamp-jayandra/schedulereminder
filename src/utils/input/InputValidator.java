package utils.input;

import utils.Utilities;
import java.util.Scanner;
import java.util.regex.*;
import java.time.LocalDateTime;

public class InputValidator{
	protected static Scanner scan = new Scanner(System.in);
	private static Pattern pattern;
	private static Matcher match;
		
	protected static int safeInteger(){
		while (true){
			try{
				return Integer.valueOf(scan.nextLine());
			}
			catch (Exception e){
				
			}
		}
	}
	
	protected static String safePathString(){
		pattern = Pattern.compile("[*?\"<>|]");
		String pathString;
		do {
			pathString = scan.nextLine();
			match = pattern.matcher(pathString);
			if (match.find()){
				System.out.printf("Path can't contain [*?\"<>|] character!\n");
				System.out.printf("Please enter new correct path: ");
			}
		}
		while (match.find());
		return pathString;
	}
	
	protected static LocalDateTime safeFormattedTime(){
		LocalDateTime time = LocalDateTime.now();
		while (true){
			String timeString = scan.nextLine();
			try{
				time = Utilities.convertStringToTime(timeString);
				break;
			}
			catch (Exception e) {
				System.out.printf("\nEnter the proper time following the example above: ");
			}
		}
		return time;
	}
}