package utils.input;

import java.util.Scanner;

public class InputReminder{
	private static Scanner scan = new Scanner(System.in);
	public static boolean inputEndConfirmation(){
		System.out.printf("\nDo you want to stop the reminder (Y/N)? ");
		String endConfirmation = "N";
		do {
			endConfirmation = scan.nextLine();
			if (!endConfirmation.equalsIgnoreCase("Y") && !endConfirmation.equalsIgnoreCase("N")){
				System.out.printf("\nEnter (Y/N) to exit the reminder: ");
			}
		}
		while (!endConfirmation.equalsIgnoreCase("Y") && !endConfirmation.equalsIgnoreCase("N"));
		return (endConfirmation.equalsIgnoreCase("Y")) ? true : false;
	}
}