package utils.input;

import constants.ConfigurationConstants;
import model.Configuration;

import java.nio.file.Files;
import java.nio.file.Paths;

public class InputConfiguration extends InputValidator{
	public static Configuration inputSpecificConfiguration(int choice, Configuration configuration){
		switch (choice){
			case 1:
				configuration = inputMusicPaths(configuration);
				break;
			case 2:
				configuration = inputReminderMinuteRunning(configuration);
				break;
		}
		return configuration;
	}
	
	public static String validateMusicPaths(String configurationValue){
		while (Files.notExists(Paths.get(configurationValue))){
			System.out.printf("\nEnter the correct .wav music path: ");
			configurationValue = safePathString();
		}
		return configurationValue;
	}
	
	public static boolean validateRepeatReminder(String configurationValue){
		while (!configurationValue.equalsIgnoreCase("true") && !configurationValue.equalsIgnoreCase("false")){
			System.out.println(configurationValue);
			System.out.printf("\nChoose only between \"TRUE\" or \"FALSE\" for repeatReminder: ");
			configurationValue = scan.nextLine();
		}
		return Boolean.valueOf(configurationValue);
	}
	
	public static int validateReminderMinuteRunning(String configurationValue){
		int configurationValueInt = 0;
		while (true){
			try {
				configurationValueInt = Integer.valueOf(configurationValue);
				if (configurationValueInt <= 0 || configurationValueInt > 30){
					System.out.printf("\nMinute limited to 1 mins - 30 mins");
					System.out.printf("\nEnter correct minute value for reminderMinuteRunning: ");
					configurationValue = String.valueOf(safeInteger());
					continue;
				}
			}
			catch (Exception e){
				System.out.printf("\nEnter the correct integer: ");
				configurationValue = String.valueOf(safeInteger());
			}
			break;
		}
		return configurationValueInt;
	}
	
	private static Configuration inputMusicPaths(Configuration configuration){
		System.out.printf("\nInput your new musicPaths (.wav): ");
		String musicPaths = safePathString();
		configuration.setMusicPaths(validateMusicPaths(musicPaths));
		return configuration;
	}
	
	private static Configuration inputReminderMinuteRunning(Configuration configuration){
		System.out.printf("\nInput your new reminderRunningMinute (1-30): ");
		String reminderRunningMinute = String.valueOf(safeInteger());
		configuration.setReminderMinuteRunning(validateReminderMinuteRunning(reminderRunningMinute));
		return configuration;
	}
}