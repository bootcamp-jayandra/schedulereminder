package utils.input;

import java.time.LocalDateTime;
import java.util.regex.*;
import utils.Utilities;

public class InputSchedule extends InputValidator{
	private static Pattern pattern;
	private static Matcher match;
	
	public static LocalDateTime inputScheduleTime(){
		System.out.printf("\nEnter the scheduled date: ");
		LocalDateTime scheduleTime = LocalDateTime.now();
		do {
			scheduleTime = safeFormattedTime();
			if (scheduleTime.isBefore(LocalDateTime.now())){
				System.out.printf("\nYou can't enter the time before now (%s)", 
					Utilities.convertTimeToString(LocalDateTime.now()));
				System.out.printf("\nEnter the proper time: ");
			}
		}
		while (scheduleTime.isBefore(LocalDateTime.now()));
		return scheduleTime;
	}
	
	public static String inputScheduleName(){
		System.out.printf("\nEnter the schedule detail: ");
		String scheduleName = "";
		pattern = Pattern.compile("[=:]");
		do {
			scheduleName = scan.nextLine();
			match = pattern.matcher(scheduleName);
			if (match.find()){
				System.out.printf("\nEnter schedule detail without [=] or [:]");
			}
		}
		while (match.find());
		return scheduleName;
	} 
}