package utils.input;

public class InputMenu extends InputValidator{
	public static int inputMenuChoice(int choiceLimit){
		int choice = 0;
		System.out.printf("Your number of choice: ");
		do {
			choice = safeInteger();
			if (choice != 99 && (choice < 0 || choice > choiceLimit)){
				System.out.printf("Input the proper number of choice: ");
			}
		}
		while (choice != 99 && (choice < 0 || choice > choiceLimit));
		return getChoiceResult(choice);
	}
	
	public static void inputBuffer(){
		System.out.printf("\n          ;;PRESS [ENTER] TO CONTINUE;;");
		scan.nextLine();
	}
	
	private static int getChoiceResult(int choice){
		if (choice == 0) {
			return -1;
		}
		return choice;
	}
}