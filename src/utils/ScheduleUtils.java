package utils;

import model.Schedule;
import utils.input.InputSchedule;
import utils.Utilities;
import java.util.regex.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.stream.Stream;
import java.time.LocalDateTime;

public class ScheduleUtils{
	private InputSchedule input;
	private Pattern pattern;
	private Matcher match;
	private List<Schedule> schedules = new ArrayList<>();
	
	public List<Schedule> getSchedules(){
		this.setUpdatedSchedulesFromFile();
		return this.schedules;
	}
	
	public void createSchedule(){
		Schedule schedule = new Schedule();
		schedule.setTime(input.inputScheduleTime());
		schedule.setName(input.inputScheduleName());
		schedules.add(schedule);
		
		pushScheduleFile();
	}
	
	public void updateSchedule(int index){
		Schedule schedule = schedules.get(index-1);
		schedule.setTime(input.inputScheduleTime());
		schedule.setName(input.inputScheduleName());
		
		pushScheduleFile();
	}
	
	public void deleteSchedule(int index){
		schedules.remove(index-1);
		pushScheduleFile();
	}
	
	private void setUpdatedSchedulesFromFile(){
		try{
			Schedule schedule = new Schedule();
			Stream<String> scheduleStream = Files.lines(
				Paths.get(schedule.getSchedulePaths()));
			scheduleStream.forEach((x) -> {
				this.addStringToSchedules(x);
			});
			pushScheduleFile();			
		}
		catch (Exception e){
			System.out.printf("\nError while getting schedule");
		}
	}
	
	private void addStringToSchedules(String lines){
		Schedule schedule = new Schedule();
		schedule.setTime(extractTime(lines));
		schedule.setName(extractName(lines));
		this.schedules.add(schedule);
	}
	
	private void pushScheduleFile(){
		Schedule schedule = new Schedule();
		removeExpiredSchedule();
		sortScheduleFile();
		try{
			Files.write(Paths.get(schedule.getSchedulePaths()), scheduleLineWriter(), StandardCharsets.UTF_8);
		}
		catch (Exception e){
			System.out.printf("\nFile not found!");
		}
	}
	
	private void removeExpiredSchedule(){
		int counter = 0;
		while (counter < schedules.size()){
			try{
				Schedule schedule = new Schedule();
				schedule = this.schedules.get(counter);
				LocalDateTime scheduleTime = schedule.getTime();
				if (scheduleTime.isBefore(LocalDateTime.now())){
					this.schedules.remove(counter);
					continue;
				}
			}
			catch (Exception e){
				break;
			}
			counter++;
		}
	}
	
	private void sortScheduleFile(){
		this.schedules.sort(new ScheduleUtils().new OrderSchedulesByTime());
	}
	
	private List<String> scheduleLineWriter(){
		List<String> scheduleLine = new ArrayList<>();
		
		for (Schedule schedule : schedules){
			String scheduleTime = Utilities.convertTimeToString(schedule.getTime());
			String schedName = schedule.getName();
			scheduleLine.add(scheduleTime+"="+schedName);
		}			
		
		return scheduleLine;
	}
	
	private LocalDateTime extractTime(String schedValue){
		pattern = pattern.compile("[0-9]{2} [A-z]{3} [0-9]{4}, [0-9]{2}\\.[0-9]{2} (PM|AM)(?==)");
		match = pattern.matcher(schedValue);
		while (match.find()){
			return Utilities.convertStringToTime(match.group());	
		}
		return LocalDateTime.now();
	}
	
	private String extractName(String schedValue){
		pattern = pattern.compile(
			"(?<=[0-9]{2} [A-z]{3} [0-9]{4}, [0-9]{2}\\.[0-9]{2} (PM|AM)=)[^=:]+");
		match = pattern.matcher(schedValue);
		while (match.find()){
			return match.group();
		}
		return "";
	}
	
	class OrderSchedulesByTime implements Comparator<Schedule>{
		@Override
		public int compare(Schedule sched1, Schedule sched2){
			LocalDateTime time1 = sched1.getTime();
			LocalDateTime time2 = sched2.getTime();
			return time1.compareTo(time2);
		}
	}
}