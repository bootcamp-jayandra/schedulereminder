package utils;

import utils.Utilities;
import utils.input.InputMenu;
import model.Configuration;
import model.Schedule;
import controller.AppController;
import view.ReminderView;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDateTime;

public class ReminderUtils{
	private Clip audioClip;
	private Configuration configuration = (new ConfigurationUtils()).getConfiguration();
	private List<Schedule> schedules = (new ScheduleUtils()).getSchedules();
	private Schedule schedule = ((new ScheduleUtils()).getSchedules()).get(0);
	private TimerTask startTask = new StartTask();
	private EndTask endTask = new EndTask();
	
	private final String scheduleName = this.schedule.getName();
	private Timer startTimer = new Timer(scheduleName);
	private Timer endTimer = new Timer("end of:"+scheduleName);
	
	public ReminderUtils(){
		this.setUpSingleScheduleReminder();
	}
	
	private void setUpSingleScheduleReminder(){
		this.startTimer.schedule(
			this.startTask, 
			Utilities.convertLocalDateTimeToDate(
				this.schedule.getTime()));
				
		
		this.endTimer.schedule(
			this.endTask,
			Utilities.convertLocalDateTimeToDate(
				(this.schedule.getTime())
				.plusMinutes(this.configuration.getReminderMinuteRunning())));
	}
	
	private Clip playReminderAudio(String musicPaths){
		File audioFile = new File(musicPaths);
		
		try {
			AudioInputStream audioInput = AudioSystem.getAudioInputStream(audioFile);
			this.audioClip = AudioSystem.getClip();
			this.audioClip.open(audioInput);
			this.audioClip.start();
			this.audioClip.loop(Clip.LOOP_CONTINUOUSLY);
			return this.audioClip;
		}
		catch (Exception e) {
			System.out.printf("\nYour music files is probably not encoded by .wav encoding");
			System.out.printf("\nPlease use another .wav file");
		}
		return null;
	}
		
	class StartTask extends TimerTask{
		public void run(){
			Configuration configuration = ReminderUtils.this.configuration;
			Schedule schedule = ReminderUtils.this.schedule;
			ReminderUtils.this.audioClip = playReminderAudio(configuration.getMusicPaths());
			ReminderView.showHappeningReminder(ReminderUtils.this.schedules, true);
		}
	}
	
	class EndTask extends TimerTask{
		public void run(){
			ReminderView.showHappeningReminder(ReminderUtils.this.schedules, false);
			ReminderUtils.this.audioClip.stop();
			ReminderUtils.this.startTimer.cancel();
			ReminderUtils.this.startTimer.purge();	
			ReminderUtils.this.endTimer.cancel();
			ReminderUtils.this.endTimer.purge();
		}		
	}
}