package utils;

import java.util.List;
import java.util.Arrays;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneId;
import java.util.Date;


public class Utilities{
	private static DateTimeFormatter dateFormat = 
		DateTimeFormatter.ofPattern("dd MMM yyyy, hh.mm a");
		
	public static String[] splitEachXthString(String string, int x){
		String[] strings = string.split(String.format("(?<=\\G.{%d})", x));
		return strings;
	}
	
	public static LocalDateTime convertStringToTime(String localDateTime){
		return LocalDateTime.parse(localDateTime, dateFormat);
	}
	
	public static String convertTimeToString(LocalDateTime localDateTime){
		return localDateTime.format(dateFormat);
	}
	
	public static Date convertLocalDateTimeToDate(LocalDateTime localDateTime){
		return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
}