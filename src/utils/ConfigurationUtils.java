package utils;

import model.Configuration;
import utils.input.InputConfiguration;
import constants.ConfigurationConstants;

import java.util.regex.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.util.stream.Stream;

public class ConfigurationUtils{
	private Pattern pattern;
	private Matcher match;
	private int counter;
	private Configuration configuration = new Configuration();
	
	public Configuration getConfiguration(){
		this.setConfigurationFromFile();
		return this.configuration;
	}
	
	public void updateConfiguration(int choice, Configuration configuration){
		counter = choice;
		configuration = InputConfiguration.inputSpecificConfiguration(choice, configuration);
		
		List<String> configurationLines = new ArrayList<>();
		configurationLines.add(ConfigurationConstants.musicPaths+"="
			+configuration.getMusicPaths());
		configurationLines.add(ConfigurationConstants.reminderMinuteRunning+"="
			+configuration.getReminderMinuteRunning());
		
		try{
			Files.write(Paths.get(configuration.getConfigurationPaths()), configurationLines, StandardCharsets.UTF_8);
		}
		catch (Exception e){
			System.out.printf("\nFile not found!");
		}
	}
	
	private void setConfigurationFromFile(){
		try{
			Stream<String> configurationStream = Files.lines(
				Paths.get(configuration.getConfigurationPaths()));
			counter = 1;
			configurationStream.forEach((x) -> {
				this.setConfiguration(x);
			});
		}
		catch (Exception e){
			System.out.printf("\nError while setting configuration");
		}
	}
	
	private void setConfiguration(String configurationLine){
		String configurationValue = getConfigurationValue(configurationLine);
		
		switch (counter){
			case 1:
				configuration.setMusicPaths(
					InputConfiguration.validateMusicPaths(configurationValue));
				break;
			case 2:
				configuration.setReminderMinuteRunning(
					InputConfiguration.validateReminderMinuteRunning(configurationValue));
				break;
		}	
		
		counter++;
	}
	
	private String getConfigurationValue(String configurationLine){
		pattern = Pattern.compile("(?<=[A-z]+=)[^*?\"<>|]+");
		match = pattern.matcher(configurationLine);
		
		while (match.find()){
			return match.group();
		}
		
		return "";
	}
}