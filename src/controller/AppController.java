package controller;

import services.ConfigurationService;
import services.ScheduleService;
import services.ReminderService;
import view.GeneralView;
import view.ReminderView;
import utils.input.InputMenu;
import utils.ScheduleUtils;
import model.Schedule;

public class AppController{
	private int flowCount = 0;
	
	public AppController(){
		this.control();
	}
	
	private void control(){
		while (flowCount < 5){
			switch (flowCount){
				case 0:
					new ReminderService();
					ReminderView.showHappeningReminder((new ScheduleUtils()).getSchedules(), false);
					int inputFlow = InputMenu.inputMenuChoice(2);
					flowCount = (inputFlow == -1 ? 0 : inputFlow);
					break;
				case 1:
					new ReminderService();
					new ConfigurationService();
					flowCount = 0;
					break;
				case 2:
					new ReminderService();
					new ScheduleService();
					flowCount = 0;
					break;
			}
		}	
		GeneralView.showGoodbyeMessage();
	}
}