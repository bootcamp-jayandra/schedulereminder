package services;

import model.Schedule;
import utils.input.InputSchedule;
import utils.input.InputMenu;
import utils.ScheduleUtils;
import view.ScheduleView;
import view.GeneralView;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDateTime;

public class ScheduleService{
	private InputSchedule input;
	private ScheduleView view;
	private int scheduleChoice;
	private int scheduleFlowCount = 0;
	
	public ScheduleService(){
		this.start();
	}
	
	private void start(){
		while (scheduleFlowCount >= 0){
			ScheduleUtils utils = new ScheduleUtils();
			List<Schedule> schedules = utils.getSchedules();
			
			switch (scheduleFlowCount){
				case 0:
					chooseSchedule(schedules);
					break;
				case 1:
					scheduleDetail(schedules);
					break;
				case 2:
					addMoreSchedule(utils);
					break;
				case 3:
					editSelectedSchedule(utils, schedules);
					break;
				case 4:
					deleteSelectedSchedule(utils, schedules);
					break;
				default:
					GeneralView.showGoodbyeMessage();
			}
		}
	}
	
	private void chooseSchedule(List<Schedule> schedules){
		view.showSchedules(schedules);
		scheduleChoice = InputMenu.inputMenuChoice(schedules.size()+1);
		
		if (scheduleChoice == -1 || scheduleChoice == 99){
			scheduleFlowCount += scheduleChoice;
			return;
		}
		
		scheduleFlowCount++;
	}
	
	private void scheduleDetail(List<Schedule> schedules){
		Schedule schedule = new Schedule();
		
		try {
			schedule = schedules.get(scheduleChoice-1);
			view.showScheduleDetail(schedule);
			scheduleFlowCount += InputMenu.inputMenuChoice(2)+1;
		}
		catch (Exception e){
			scheduleFlowCount = 2;
		}
	}
	
	private void addMoreSchedule(ScheduleUtils utils){
		view.showReviseSchedule();
		utils.createSchedule();
		scheduleFlowCount = 0;
	}
	
	private void editSelectedSchedule(ScheduleUtils utils, List<Schedule> schedules){
		view.showReviseSchedule();
		if (schedules.size() != 0){
			utils.updateSchedule(scheduleChoice);
		}
		scheduleFlowCount = 1;
	}
	
	private void deleteSelectedSchedule(ScheduleUtils utils, List<Schedule> schedules){
		if (schedules.size() != 0){
			utils.deleteSchedule(scheduleChoice);
		}
		scheduleFlowCount = 0;
	}	
}