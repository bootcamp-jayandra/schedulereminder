package services;

import utils.ReminderUtils;
import utils.ConfigurationUtils;
import utils.ScheduleUtils;
import model.Configuration;
import model.Schedule;
import view.ReminderView;
import java.util.List;
import java.util.ArrayList;
import java.util.TimerTask;

public class ReminderService{
	
	public ReminderService(){
		this.start();
	}
	
	private void start(){
		List<Schedule> schedules = (new ScheduleUtils()).getSchedules();
		if (schedules.size() != 0){
			ReminderView.showHappeningReminder(schedules, false);
			ReminderUtils utils = new ReminderUtils();
		}
	}
}