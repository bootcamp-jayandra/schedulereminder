package services;

import model.Configuration;
import utils.input.InputConfiguration;
import utils.input.InputMenu;
import utils.ConfigurationUtils;
import view.ConfigurationView;
import view.GeneralView;
import view.ReminderView;

public class ConfigurationService extends ConfigurationUtils{
	private InputConfiguration input;
	private ConfigurationView view;
	private int configurationChoice;
	private int configurationFlowCount = 0;
	
	public ConfigurationService(){
		this.start();
	}
	
	public void start(){
		while (configurationFlowCount>=0){
			ConfigurationUtils utils = new ConfigurationUtils();
			Configuration configuration = utils.getConfiguration();
			switch (configurationFlowCount){
				case 0:
					fullConfiguration(configuration);
					break;
				case 1:
					chooseConfiguration(configuration);
					break;
				case 2:
					editConfiguration(utils, configuration);
					break;
				default:
					GeneralView.showGoodbyeMessage();
			}
		}
	}
	
	private void fullConfiguration(Configuration configuration){
		view.showConfigurationLists(configuration);
		configurationChoice = InputMenu.inputMenuChoice(2);
		
		if (configurationChoice != -1 && configurationChoice != 99){
			configurationFlowCount++;
		}
		else {
			configurationFlowCount += configurationChoice;
		}
	}
	
	private void chooseConfiguration(Configuration configuration){
		view.showSpecificConfiguration(configurationChoice);
		configurationFlowCount += InputMenu.inputMenuChoice(1);
	}
	
	private void editConfiguration(ConfigurationUtils utils, Configuration configuration){
		view.showPastSpecificConfiguration(configurationChoice, configuration);
		utils.updateConfiguration(configurationChoice, configuration);
		configurationFlowCount = 0;
		InputMenu.inputBuffer();
	} 
}