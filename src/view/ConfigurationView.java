package view;

import model.Configuration;
import utils.Utilities;

public class ConfigurationView extends GeneralView{
	public static void showConfigurationLists(Configuration configuration){
		String[] musicPaths = Utilities.splitEachXthString(configuration.getMusicPaths(), 45);
		int reminderMinuteRunning = configuration.getReminderMinuteRunning();
		
		showAppDescription();
		System.out.printf("|                                               |\n");
		System.out.printf("|            ;;USER CONFIGURATION;;             |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("| (1) musicPaths:                               |\n");
		for (String path : musicPaths){
			System.out.printf("| %-45s |\n", path);
		}
		System.out.printf("|                                               |\n");
		System.out.printf("| (2) reminderMinuteRunning: %-18d |\n", configuration.getReminderMinuteRunning()); 
		System.out.printf("|                                               |\n");
		MenuView.showGeneralMenu();
	}
	
	public static void showSpecificConfiguration(int choice){
		showAppDescription();
		switch (choice){
			case 1:
				showMusicPathsConfiguration();
				break;
			case 2:
				showReminderMinuteRunning();
				break;
		}
		MenuView.showConfigurationMenu();
	}
	
	public static void showPastSpecificConfiguration(int choice, Configuration configuration){
		showAppDescription();
		switch (choice){
			case 1:
				showPastMusicPathsConfiguration(Utilities.splitEachXthString(configuration.getMusicPaths(), 45));
				break;
			case 2:
				showPastReminderMinuteRunning(configuration.getReminderMinuteRunning());
				break;
		}
		showBottomBorder();
	}
	
	private static void showPastMusicPathsConfiguration(String[] musicPaths){
		System.out.printf("| musicPaths:                                   |\n");
		for (String path : musicPaths){
			System.out.printf("| %-45s |\n", path);
		}
		System.out.printf("|                                               |\n");
		showMusicPathsConfiguration();
	}
	
	private static void showPastReminderMinuteRunning(int reminderMinuteRunning){
		System.out.printf("| reminderMinuteRunning: %-22d |\n", reminderMinuteRunning);
		System.out.printf("|                                               |\n");
		showReminderMinuteRunning();
	}
	
	private static void showMusicPathsConfiguration(){
		System.out.printf("| ;;About musicPaths;;                          |\n");
		System.out.printf("| Path where you store your music which'll be   |\n");
		System.out.printf("| played when there were any schedules for you  |\n");
		System.out.printf("| [Drives:/specific/path to/musicFiles.wav]     |\n");
	}
	
	private static void showReminderMinuteRunning(){
		System.out.printf("| ;;About reminderMinuteRunning;;               |\n");
		System.out.printf("| The reminder will be repeated for each x      |\n");
		System.out.printf("| minutes in 1 hour [integer minute (1-30)]     |\n");
	}
}
