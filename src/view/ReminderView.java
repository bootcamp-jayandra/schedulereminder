package view;

import utils.Utilities;
import model.Schedule;
import view.GeneralView;
import view.MenuView;
import java.time.LocalDateTime;
import java.util.List;
import java.util.ArrayList;

public class ReminderView extends GeneralView{
	public static void showHappeningReminder(List<Schedule> schedules, boolean isHappening){
		GeneralView.showAppDescription();
		if (isHappening){
			System.out.printf("|                 ;;NOW HAPPEN;;                |\n");
			showScheduleReminderDetail(schedules.get(0));
			System.out.printf("|                   ;;ON NEXT;;                 |\n");
			try {
				showScheduleReminderDetail(schedules.get(1));
			}
			catch (Exception e){
				System.out.printf("| Hooray! there are no more schedule for you!   |\n");
			}
		}
		else if (!isHappening){
			System.out.printf("|                   ;;ON NEXT;;                 |\n");
			if (schedules.isEmpty()){
				System.out.printf("| Hooray! there are no more schedule for you!   |\n");
			}
			else {
				showScheduleReminderDetail(schedules.get(0));
			}
		}
		MenuView.showMainMenu();
	}
	
	private static void showScheduleReminderDetail(Schedule schedule){
		LocalDateTime scheduleTime = schedule.getTime();
		String[] scheduleName = Utilities.splitEachXthString(schedule.getName(), 45);
		System.out.printf("|                                               |\n");
		System.out.printf("| %-45s |\n", Utilities.convertTimeToString(scheduleTime));
		for (String string : scheduleName){
			System.out.printf("| %-45s |\n", string);
		}
	}
}