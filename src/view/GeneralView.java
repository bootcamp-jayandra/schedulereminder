package view;

public class GeneralView {
	public static void showGoodbyeMessage(){
		showAppDescription();
		System.out.printf("|                                               |\n");
		System.out.printf("|          The application is stopped!          |\n");
		System.out.printf("|     Thank you for using our application!      |\n");
		showBottomBorder();	
		System.exit(0);
	}
	
	protected static void showAppDescription(){
		clearTerminalDisplay();
		System.out.printf("=================================================\n");
		System.out.printf("|                                               |\n");
		System.out.printf("|             SCHEDULE REMINDER APP             |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("| Use this app to prevent you from forgetting   |\n");
		System.out.printf("| your own schedule!                            |\n");
		System.out.printf("|                                               |\n");
	}
	
	protected static void showBottomBorder(){
		System.out.printf("|                                               |\n");
		System.out.printf("=================================================\n");
	}
	
	public static void clearTerminalDisplay(){
		try {
			if (System.getProperty("os.name").contains("Windows")){
				new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
			}
			else {
				System.out.printf("Sorry this app only work on Windows CMD!");
			}
		}
		catch (Exception exception){
			System.out.printf("Sorry this app only work on Windows CMD!");
		}
	}
}