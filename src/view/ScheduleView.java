package view;

import model.Schedule;
import utils.ScheduleUtils;
import utils.Utilities;
import java.util.List;

public class ScheduleView extends GeneralView{
	public static void showSchedules(List<Schedule> schedules){
		showAppDescription();
		System.out.printf("|              ;;USER SCHEDULES;;               |\n");
		if (schedules.size() != 0){
			for (int schedCounter = 1; schedCounter <= schedules.size(); schedCounter++){
				Schedule schedule = schedules.get(schedCounter-1);
				String[] schedName = Utilities.splitEachXthString(schedule.getName(), 41);
				String schedTime = Utilities.convertTimeToString(schedule.getTime());
				System.out.printf("|                                               |\n");
				System.out.printf("| %2d) %-41s |\n", schedCounter, schedTime);
				for (String string : schedName){
					System.out.printf("|     %-41s |\n", string);
				}
			}
		}
		System.out.printf("|                                               |\n");
		System.out.printf("| (%d) Add schedule                             %s|\n",
			schedules.size()+1, 
			((String.valueOf(schedules.size()+1)).length() > 1 ? "" : " "));
		MenuView.showGeneralMenu();
	}
	
	public static void showScheduleDetail(Schedule schedule){
		showAppDescription();
		String schedTime = Utilities.convertTimeToString(schedule.getTime());
		String[] schedName = Utilities.splitEachXthString(schedule.getName(), 41);
		System.out.printf("| Scheduled time: %-29s |\n", schedTime);
		for (String string : schedName){
			System.out.printf("|     %-41s |\n", string);
		}
		System.out.printf("|                                               |\n");
		MenuView.showEditScheduleMenu();
	}
	
	public static void showReviseSchedule(){
		showAppDescription();
		System.out.printf("| Please enter your scheduled time and the      |\n");
		System.out.printf("| schedule detail!                              |\n");
		System.out.printf("|                                               |\n");
		System.out.printf("| *Schedule time needs to meet format:          |\n");
		System.out.printf("|  dd MMM yyyy, hh.mm aa                        |\n");
		System.out.printf("|  Example: 30 Jul 2021, 01.00 AM               |\n");
		showBottomBorder();
	}
}