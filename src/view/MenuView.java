package view;

public class MenuView extends GeneralView{
	public static void showMainMenu(){
		System.out.printf("|                                               |\n");
		System.out.printf("| (1) See configuration                         |\n");
		System.out.printf("| (2) View schedule                             |\n");
		showGeneralMenu();
	}
	
	public static void showConfigurationMenu(){
		System.out.printf("|                                               |\n");
		System.out.printf("| (1) Edit configuration                        |\n");
		showGeneralMenu();
	}
	
	public static void showEditScheduleMenu(){
		System.out.printf("| (1) Edit schedule                             |\n");
		System.out.printf("| (2) Delete this schedule                      |\n");
		showGeneralMenu();
	}
	
	public static void showGeneralMenu(){
		System.out.printf("| (0) Back                                      |\n");
		System.out.printf("| (99) Close this app                           |\n");
		showBottomBorder();
	}
}