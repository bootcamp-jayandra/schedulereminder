package model;

import java.nio.file.Paths;
import java.nio.file.Files;

public class Directory{
	private final String assetDirectory = (Paths.get("").toAbsolutePath().toString())
		.replace("\\src", "\\assets");
		
	protected String getAssetDirectory(){
		return this.assetDirectory;
	}	
}