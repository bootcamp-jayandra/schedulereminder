package model;

import model.Directory;

public class Configuration extends Directory{
	private final String configurationPaths = super.getAssetDirectory() + "\\conf.txt";
	private String musicPaths;
	private int reminderMinuteRunning;
	
	public String getConfigurationPaths(){
		return this.configurationPaths;
	}
	
	public void setMusicPaths(String musicPaths){
		this.musicPaths = musicPaths;
	}
	
	public String getMusicPaths(){
		return this.musicPaths;
	}
	
	public void setReminderMinuteRunning(int reminderMinuteRunning){
		this.reminderMinuteRunning = reminderMinuteRunning;
	}
	
	public int getReminderMinuteRunning(){
		return this.reminderMinuteRunning;
	}
}