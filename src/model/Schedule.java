package model;

import java.time.LocalDateTime;

public class Schedule extends Directory{
	private final String schedulePaths = super.getAssetDirectory() + "\\schedule.txt";
	private String name;
	private LocalDateTime time;
	
	public String getSchedulePaths(){
		return this.schedulePaths;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setTime(LocalDateTime time){
		this.time = time;
	}
	
	public LocalDateTime getTime(){
		return this.time;
	}
	
}