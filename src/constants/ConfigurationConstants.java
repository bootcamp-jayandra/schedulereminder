package constants;

public class ConfigurationConstants{
	public static final String musicPaths = "musicPaths";
	public static final String repeatReminder = "repeatReminder";
	public static final String reminderMinuteRunning = "reminderMinuteRunning";
}